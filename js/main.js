var todosFrontEndApp = angular.module('todosFrontEnd', []);

todosFrontEndApp.controller('DataFromApi', function ($scope, $http) {

    $scope.doneWell = {
        "background-color": "#639a67"
    }


    $http.get("https://stormy-shore-12673.herokuapp.com/").then((response) => {
        console.log(response.data);
        $scope.todos = response.data.reverse();
        if ($scope.todos.length == 0) {
            $scope.todosNotFound = "No todos here! Add some...";
        }
    }).catch((error) => {
        console.log(error);
    });

    let dateFormatter = function (date) {
        let day = ("0" + date.getDate()).slice(-2);
        let month = ("0" + (date.getMonth() + 1)).slice(-2);
        let formattedDate = date.getFullYear() + "-" + (month) + "-" + (day);

        return formattedDate;
    }

    $scope.postTodo = function (todoDesc, todoDate) {
        let formattedDate = dateFormatter(todoDate);
        var newTodo = {
            "todoDescription": todoDesc,
            "createdAt": formattedDate,
            "isDone": false
        };
        $http.post("https://stormy-shore-12673.herokuapp.com/", newTodo).then((response) => {
            $scope.todos = response.data.reverse();
        }).catch((error) => {
            console.log(error);
        });
        document.getElementById("createTodo").reset();
        var createdTodoToast = document.getElementById("createdToast");
        createdTodoToast.className = "show";
        setTimeout(function () { createdTodoToast.className = createdTodoToast.className.replace("show", ""); }, 3000);

    };

    $scope.deleteTodo = function (todo) {
        $http.delete("https://stormy-shore-12673.herokuapp.com/del/" + todo.id).then((response) => {
            console.log(response);
            $http.get("https://stormy-shore-12673.herokuapp.com/").then((response) => {
                console.log(response.data);
                $scope.todos = response.data.reverse();
                if ($scope.todos.length == 0) {
                    $scope.todosNotFound = "No todos here! Add some...";
                }
            }).catch((error) => {
                console.log(error);
            });
        });
        var deleteTodoToast = document.getElementById("deleteToast");
        deleteTodoToast.className = "show";
        setTimeout(function () { deleteTodoToast.className = deleteTodoToast.className.replace("show", ""); }, 5000);
    };

    $scope.editTodo = function (todo) {
        let url = "https://stormy-shore-12673.herokuapp.com/edit/" + todo.id;
        $http.put(url, todo).then((response) => {
            // console.log(todo);
            console.log(response);
        });
    };

});
